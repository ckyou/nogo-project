package nogo.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Scanner;

import nogo.mct.Location;
import nogo.mct.TreeNode;
import nogo.mct.UCTSearch;
import nogo.mct.utility.MoveTable;
import nogo.mct.utility.moveTableGenerator;
import nogo.utility.CheckLiberty;
import nogo.utility.Constant;
import nogo.utility.Utility;

public class Main {

	static char[][] board;
	static int row = Constant.DEFAULT_ROW_SIZE;
	static int col = Constant.DEFAULT_COL_SIZE;
	static int numOfBlackPiece, numOfWhitePiece;
	
	static boolean OUTPUT_TO_FILE = true;
	static boolean INPUT_FROM_FILE = true;
	static String filePath = "./";
	static String fileName = "0156026.txt";
	static DecimalFormat df=new DecimalFormat("#.####");
	
	public static void main(String[] args) {	
		getInput(OUTPUT_TO_FILE);
	}
	
	public static void getInput(boolean toFile){
		Scanner sc=new Scanner(System.in);
		FileWriter fwriter = null;
		try {
			fwriter = new FileWriter(new File(filePath+fileName));
		} catch (IOException e) {
			e.printStackTrace();
		};

		String inputFilePath;
		if(INPUT_FROM_FILE){
			System.out.println("get input file> ");
			inputFilePath = sc.nextLine();
			try {
				sc= new Scanner(new File(inputFilePath));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		while(true){
			
			/* get input */
			String pattern = null;
			if(INPUT_FROM_FILE){
				if(!sc.hasNextLine())
					break;
				pattern = sc.nextLine();
			}else{
				System.out.print("> ");
				pattern = sc.nextLine();
				if(pattern.equals("exit")) break;
			}

			/* init board*/
			fillInBoard(pattern.trim(), row, col);
			
			/* check input is illegal or not*/
			//I believe the input given by TA is always correct!
			//CheckLiberty.isLegalBoard(board, row, col);
			
			/* build root node*/
			TreeNode root;
			if (numOfBlackPiece > numOfWhitePiece)
				root = new TreeNode(new Location(-1, -1), Constant.WHITE);
			else
				root = new TreeNode(new Location(-1, -1), Constant.BLACK);

			/* UCT search */
			UCTSearch.apply(root, board);
	
			/* print the result */
			if(root.visitedTime!=0){
				if(toFile){
					Float output = new Float(root.winTime/root.visitedTime);
					try {
						fwriter.append(df.format(output)+"\r\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				else
					System.out.printf("%.4f\n", root.winTime/root.visitedTime);
			}
			else{
				if(toFile){
					Float output = new Float(root.winTime/root.visitedTime);
					try {
						fwriter.append(df.format(output)+"\r\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				else
					System.out.printf("%.4f\n", 0.0f);
			}
		}

		
		try {
			if(fwriter!=null)
				fwriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("bye bye~");
		sc.close();
	}	
	
	public static void fillInBoard(String pattern, int row, int col){
		board = new char[row][col];
		numOfBlackPiece = 0;
		numOfWhitePiece = 0;

		int k=0;
		for(int i=0; i<row; ++i){
			for(int j=0; j<col; ++j){
				board[i][j] = pattern.charAt(k);
				
				if (pattern.charAt(k) == Constant.BLACK_PIECE)
					++numOfBlackPiece;
				else if (pattern.charAt(k) == Constant.WHITE_PIECE)
					++numOfWhitePiece;
				
				++k;
				
				if(board[i][j]==' '){
					try {
						throw new Exception("input format error!");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			++k; //jump one character;
		}
	}
		
}
