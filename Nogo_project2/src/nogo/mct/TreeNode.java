package nogo.mct;

import java.util.ArrayList;

public class TreeNode {
	public Location location;
	public int player; 
	
	public float visitedTime;
	public float winTime;
	public float visitedTime_RAVE;
	public float winTime_RAVE;

	public ArrayList<TreeNode> children;
	public int child2GoNum, child1GoNum;
	
	
	public TreeNode parent;

	public TreeNode(Location loc, int player) {
		location = new Location(loc.x, loc.y, loc.type);
		this.player = player;
		visitedTime = 0;
		winTime = 0;
		visitedTime_RAVE = 0;
		winTime_RAVE = 0;

		children = new ArrayList<>();
		child2GoNum = 0;
		child1GoNum = 0;
		parent = null;
	}
	
	public boolean isChild(Location target){
		for(int i=0; i<children.size(); ++i){
			if(children.get(i).location.equals(target))
				return true;
		}
		return false;
	}
}
