package nogo.mct;

import java.util.Random;

import nogo.mct.utility.MoveTable;
import nogo.mct.utility.moveTableGenerator;
import nogo.utility.CheckLiberty;
import nogo.utility.Constant;
import nogo.utility.Utility;

public class UCTSearch {
	public static final int MAX_ITERATION_TIME = 1000; // in millisecond
	public static final double SQRT_2 = 1.414; 

	private static MoveTable moveTable;
	private static Random random;
	private static long beginTime, endTime;
	private static TreeNode rootNode;

	public static void apply(TreeNode root, char[][] board) {
		random = new Random();

		beginTime = System.currentTimeMillis();
		endTime = beginTime + MAX_ITERATION_TIME;

		moveTable = moveTableGenerator.gen(board);
		rootNode = root;

		while (System.currentTimeMillis() < endTime) {
			TreeNode target = getTarget(board, root);
			if (target == null)
				break;

			int whoWin = playout(target, board);
			int result;
			if (whoWin == root.player)
				result = 1;
			else
				result = 0;
			backPropagation(target, result);
		}
	}

	private static TreeNode getTarget(char[][] board, TreeNode pointer) {
		TreeNode target = null;

		if (pointer == null)
			return target;

		if (pointer.child2GoNum < moveTable.move2Go.size()) {
			int index = 0;
			boolean finded = false;
			while (!finded && System.currentTimeMillis() < endTime) {
				index = random.nextInt(moveTable.move2Go.size());

				board[moveTable.move2Go.get(index).x][moveTable.move2Go
						.get(index).y] = Utility.getPiece(Utility
						.opponent(pointer.player));
				if (CheckLiberty.isLegalBoard(board, Constant.DEFAULT_ROW_SIZE,
						Constant.DEFAULT_COL_SIZE))
					finded = true;
				board[moveTable.move2Go.get(index).x][moveTable.move2Go
						.get(index).y] = Constant.EMPTY_PIECE;
			}
			target = expand(pointer, moveTable.move2Go.get(index));

		} else if (pointer.child1GoNum < moveTable.move1Go.size()) {
			int index = 0;
			boolean finded = false;
			while (!finded && System.currentTimeMillis() < endTime) {
				index = random.nextInt(moveTable.move1Go.size());

				board[moveTable.move1Go.get(index).x][moveTable.move1Go
						.get(index).y] = Utility.getPiece(Utility
						.opponent(pointer.player));
				if (CheckLiberty.isLegalBoard(board, Constant.DEFAULT_ROW_SIZE,
						Constant.DEFAULT_COL_SIZE))
					finded = true;
				board[moveTable.move1Go.get(index).x][moveTable.move1Go
						.get(index).y] = Constant.EMPTY_PIECE;
			}
			target = expand(pointer, moveTable.move1Go.get(index));
		} else {
			moveTable.removeNode(pointer.location);
			target = getTarget(board, findBestChild(pointer));
			moveTable.addNode(pointer.location);
		}

		return target;
	}

	private static TreeNode findBestChild(TreeNode pointer) {
		TreeNode bestChild = null;
		double bestUCT = 0;
		double tmp = 0;

		for (int i = 0; i < pointer.children.size(); ++i) {
			tmp = pointer.children.get(i).winTime
					/ pointer.children.get(i).visitedTime 
					+ SQRT_2 * Math.sqrt(Math.log(rootNode.visitedTime)/pointer.children.get(i).visitedTime) ;
			if (tmp >= bestUCT) {
				bestChild = pointer.children.get(i);
				bestUCT = tmp;
			}
		}
		return bestChild;
	}

	private static TreeNode expand(TreeNode parent, Location move) {
		TreeNode target = new TreeNode(move, Utility.opponent(parent.player));
		target.parent = parent;
		parent.children.add(target);

		if (move.type == Location.TYPE_2GO) {
			++parent.child2GoNum;

		} else if (move.type == Location.TYPE_1GO) {
			++parent.child1GoNum;
		}

		return target;
	}

	private static int playout(TreeNode fromThisNode, char[][] board) {
		int whoWin = fromThisNode.player;
		TreeNode pointer = fromThisNode;
		/* setup board */
		while (pointer.parent != null) {
			board[pointer.location.x][pointer.location.y] = Utility
					.getPiece(pointer.player);
			pointer = pointer.parent;
		}

		whoWin = _playout(board, Utility.opponent(pointer.player));
		
		/* reset board */
		pointer = fromThisNode;
		while (pointer.parent != null) {
			board[pointer.location.x][pointer.location.y] = Constant.EMPTY_PIECE;
			pointer = pointer.parent;
		}

		return whoWin;
	}

	private static int _playout(char[][] board, int player) {
		int whoWin = Utility.opponent(player);
		int x, y;
		boolean finded = false;
		int times=0;
		
		while (!finded && System.currentTimeMillis() < endTime) {
			x = random.nextInt(Constant.DEFAULT_ROW_SIZE);
			y = random.nextInt(Constant.DEFAULT_COL_SIZE);

			if (board[x][y] == Constant.EMPTY_PIECE) {
				board[x][y] = Utility.getPiece(Utility.opponent(player));
				if (CheckLiberty.isLegalBoard(board, Constant.DEFAULT_ROW_SIZE,
						Constant.DEFAULT_COL_SIZE)) {
					finded = true;
					whoWin = _playout(board, Utility.opponent(player));
				}
				
				++times;
				if(times>6){
					finded = true;
					whoWin = player;
				}
					
				board[x][y] = Constant.EMPTY_PIECE;
			}
		}

		return whoWin;
	}

	private static void backPropagation(TreeNode poiner, int result) {
		if (poiner != null) {
			++poiner.visitedTime;
			if (result != 0)
				++poiner.winTime;
			backPropagation(poiner.parent, result);
		}
	}
}
