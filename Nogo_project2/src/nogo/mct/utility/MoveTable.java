package nogo.mct.utility;

import java.util.ArrayList;

import nogo.mct.Location;
import nogo.mct.TreeNode;

public class MoveTable {
	public ArrayList<Location> move2Go;
	public ArrayList<Location> move1Go;
	public MoveTable(){
		move2Go = new ArrayList<>();
		move1Go = new ArrayList<>();
	}
	
	public boolean move2GoContain(Location target){
		for(int i=0; i<move2Go.size(); ++i){
			if(move2Go.get(i).equals(target))
				return true;
		}
		return false;
	}
	
	public boolean move1GoContain(Location target){
		for(int i=0; i<move1Go.size(); ++i){
			if(move1Go.get(i).equals(target))
				return true;
		}
		return false;
	}
	
	public void removeNode(Location target){
		if(target.type == Location.TYPE_2GO){
			for(int i=0; i<move2Go.size(); ++i){
				if(move2Go.get(i).equals(target))
					move2Go.remove(i);
			}
		}else if(target.type == Location.TYPE_1GO){
			for(int i=0; i<move1Go.size(); ++i){
				if(move1Go.get(i).equals(target))
					move1Go.remove(i);
			}
		}
	}
	
	public void addNode(Location target){
		if(target.type == Location.TYPE_2GO)
			move2Go.add(target);
		else if(target.type == Location.TYPE_1GO)
			move1Go.add(target);
	}
}

