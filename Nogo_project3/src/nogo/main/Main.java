package nogo.main;

import java.util.HashMap;
import java.util.Scanner;

import nogo.mct.Location;
import nogo.mct.TreeNode;
import nogo.mct.UCTSearch;
import nogo.utility.Constant;

public class Main {

	static char[][] board;
	static int row = Constant.DEFAULT_ROW_SIZE;
	static int col = Constant.DEFAULT_COL_SIZE;
	static int numOfBlackPiece, numOfWhitePiece;
	
	final static HashMap<Character, Integer> XReverseMapping;
	static{
		XReverseMapping = new HashMap<>();
		XReverseMapping.put('A', 1);
		XReverseMapping.put('B', 2);
		XReverseMapping.put('C', 3);
		XReverseMapping.put('D', 4);
		XReverseMapping.put('E', 5);
		XReverseMapping.put('F', 6);
		XReverseMapping.put('G', 7);
		XReverseMapping.put('H', 8);
		XReverseMapping.put('J', 9);
	}
	
	final static HashMap<Integer, Character> XMapping;
	static{
		XMapping = new HashMap<>();
		XMapping.put(1, 'A');
		XMapping.put(2, 'B');
		XMapping.put(3, 'C');
		XMapping.put(4, 'D');
		XMapping.put(5, 'E');
		XMapping.put(6, 'F');
		XMapping.put(7, 'G');
		XMapping.put(8, 'H');
		XMapping.put(9, 'J');
	}
		
	public static void main(String[] args) {	
		getInput();
	}
	
	public static void getInput(){
		Scanner sc=new Scanner(System.in);

		/* init board*/
		_initBoard();
		String input = null;
		
		while(true)
		{

			/* get input */
			System.out.print("> ");
			input = sc.nextLine();
			
			if(input.equals("exit")){ 
				break;
			}else if(input.equals("reset")){
				_initBoard();
			}
			else{
				String[] tokens = input.split(" ");
				
				if(tokens[0].equals("play") && tokens.length == 3){
					int x = XReverseMapping.get(tokens[2].charAt(0)) - 1, 
						y = Integer.parseInt("" + tokens[2].charAt(1)) - 1;
					
					if(board[x][y] != Constant.EMPTY_PIECE){
						System.out.println("This location is not empty");
						continue;
					}
					
					if(tokens[1].equals("black")){
						board[x][y] = Constant.BLACK_PIECE;
						++numOfBlackPiece;
					}else if(tokens[1].equals("white")){
						board[x][y] = Constant.WHITE_PIECE;
						++numOfWhitePiece;
					}else{
						System.out.println("Syntax ERROR");
					}
					
					if(!_isPieceNumberLegal()){
						System.out.println("Sequnce is wrong!!");
						System.out.println("-> undo this move");
						board[x][y] = Constant.EMPTY_PIECE;
						if(tokens[1].equals("black")){
							--numOfBlackPiece;
						}else if(tokens[1].equals("white")){
							--numOfWhitePiece;
						}
					}

				}else if(tokens[0].equals("genmove") && tokens.length == 2){
					//WARNING: I didn't prevent user to genmove while this board is its turn or not
					
					TreeNode root;
					if(tokens[1].equals("black")){
						root = new TreeNode(new Location(-1, -1), Constant.BLACK);
						UCTSearch.apply(root, board);
						System.out.println("="+getBestMove(root));
					}else if(tokens[1].equals("white")){
						root = new TreeNode(new Location(-1, -1), Constant.WHITE);
						UCTSearch.apply(root, board);
						System.out.println("="+getBestMove(root));
					}else{
						System.out.println("Syntax ERROR");
					}
				}else{
					System.out.println("Syntax ERROR");
				}
			}	
		}
		
		System.out.println("bye bye~");
		sc.close();
	}	
	
	
	public static String getBestMove(TreeNode root){
		float score=0, tmpScore=0;
		float bestMoveNum=0;
		
		Location bestMove = null;
		for(int i=0; i<root.children.size(); ++i){
			tmpScore = root.children.get(i).winTime/root.children.get(i).visitedTime;
//			if(tmpScore > score ){
//				score = tmpScore;
//				bestMove = root.children.get(i).location;
//			}
			if(root.children.get(i).visitedTime > bestMoveNum){
				bestMoveNum = root.children.get(i).visitedTime;
				bestMove = root.children.get(i).location;
				//System.out.println(""+bestMove.x+"/"+bestMove.y+" "+bestMoveNum);
			}
		}
		
		if(bestMove == null)
			return "ERROR";
		else{
			return ""+XMapping.get(bestMove.x+1)+(bestMove.y+1);
		}
	}
	
	private static boolean _isPieceNumberLegal(){
		if(numOfBlackPiece < numOfWhitePiece)
			return false;
		
		if(numOfBlackPiece > numOfWhitePiece+1)
			return false;
		
		return true;
	}
	
	private static void _initBoard(){
		board = new char[row][col];
		numOfBlackPiece = 0;
		numOfWhitePiece = 0;
		for(int i=0; i<row; ++i)
			for(int j=0; j<col; ++j)
				board[i][j] = Constant.EMPTY_PIECE;
	}
	
	public static void fillInBoard(String pattern, int row, int col){
		board = new char[row][col];
		numOfBlackPiece = 0;
		numOfWhitePiece = 0;

		int k=0;
		for(int i=0; i<row; ++i){
			for(int j=0; j<col; ++j){
				board[i][j] = pattern.charAt(k);
				
				if (pattern.charAt(k) == Constant.BLACK_PIECE)
					++numOfBlackPiece;
				else if (pattern.charAt(k) == Constant.WHITE_PIECE)
					++numOfWhitePiece;
				
				++k;
				
				if(board[i][j]==' '){
					try {
						throw new Exception("input format error!");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			++k; //jump one character;
		}
	}
		
}
