package nogo.mct.utility;

import nogo.mct.Location;
import nogo.utility.Constant;

public class moveTableGenerator {

	public static MoveTable gen(char[][] board) {
		MoveTable table = new MoveTable();

		for (int i = 0; i < Constant.DEFAULT_ROW_SIZE; ++i) {
			for (int j = 0; j < Constant.DEFAULT_COL_SIZE; ++j) {
				if (board[i][j] == Constant.EMPTY_PIECE) {
					int type = judgeMoveType(board, i - 1, i + 1, j - 1, j + 1);

					if (type == Location.TYPE_2GO)
						table.move2Go.add(new Location(i, j, type));
					else
						table.move1Go.add(new Location(i, j, type));
					
					//System.out.print(type);
				}else{
					//do nothing
					//System.out.print('x');
				}
				
			}
			//System.out.println();
		}
		return table;
	}

	private final static char WALL = '|';

	public static int judgeMoveType(char[][] board, int up, int down, int left,
			int right) {
		
		/* get each piece at each position */
		char upPiece, downPiece, rightPiece, leftPiece;
		if (up > 0)
			upPiece = board[up][left + 1];
		else
			upPiece = WALL;
		
		if (down < Constant.DEFAULT_ROW_SIZE)
			downPiece = board[down][left + 1];
		else
			downPiece = WALL;
		
		if(left > 0)
			leftPiece = board[up + 1][left];
		else
			leftPiece = WALL;
					
		if(right < Constant.DEFAULT_COL_SIZE)
			rightPiece = board[up + 1][right];
		else
			rightPiece = WALL;

		/* return type */
		
		if (leftPiece != Constant.EMPTY_PIECE && leftPiece != WALL)
			return Location.TYPE_1GO;
		if (rightPiece != Constant.EMPTY_PIECE && rightPiece != WALL)
			return Location.TYPE_1GO;
		if (upPiece != Constant.EMPTY_PIECE && upPiece != WALL)
			return Location.TYPE_1GO;
		if (downPiece != Constant.EMPTY_PIECE && downPiece != WALL)
			return Location.TYPE_1GO;

		return Location.TYPE_2GO;
	}
}
