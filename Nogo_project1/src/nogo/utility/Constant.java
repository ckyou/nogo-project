package nogo.utility;

public class Constant {
	public static int BLACK = 1;
	public static int WHITE = 2;
	public static int EMPTY = 0;

	public static char BLACK_PIECE = '@';
	public static char WHITE_PIECE = 'O';
	public static char EMPTY_PIECE = '.';

}
