package nogo.utility;

public class CheckLiberty {
	private static byte[][] libretyTable;
	
	private final static byte UNMARKED = 0; 
	private final static byte MARKED = 1; 
	
	private static boolean BLACK_IS_DEAD, WHITE_IS_DEAD;
	
	private static int numBlackPiece, numWhitePiece, numEmpty;
		
	public static boolean isLegalBoard(char[][] board, int row, int col){
		return isLegalBoard(board, row, col, false);
	}
	
	public static boolean isLegalBoard(char[][] board, int row, int col, boolean getMoreDetail){
		numBlackPiece=0;
		numWhitePiece=0;
		numEmpty=0;
		BLACK_IS_DEAD=false; 
		WHITE_IS_DEAD=false;
		libretyTable = new byte[row][col];
		
		for(int i=0; i<row; ++i)
			for(int j=0; j<col; ++j){
				libretyTable[i][j] = UNMARKED;
				if(board[i][j] == Constant.BLACK_PIECE)
					++numBlackPiece;
				else if(board[i][j] == Constant.WHITE_PIECE)
					++numWhitePiece;
				else
					++numEmpty;
			}
				
		for(int i=0; i<row; ++i)
			for(int j=0; j<col; ++j){
				if(board[i][j] == Constant.EMPTY_PIECE){
					libretyTable[i][j] = MARKED;
					flow(board, row, col, i+1, j, Constant.EMPTY_PIECE);
					flow(board, row, col, i-1, j, Constant.EMPTY_PIECE);
					flow(board, row, col, i, j+1, Constant.EMPTY_PIECE);
					flow(board, row, col, i, j-1, Constant.EMPTY_PIECE);
				}
			}
		
		for(int i=0; i<row; ++i)
			for(int j=0; j<col; ++j){
				if(libretyTable[i][j] == UNMARKED){
					if(!getMoreDetail)
						return false;
					else{
						if(board[i][j] == Constant.BLACK_PIECE)
							BLACK_IS_DEAD = true;
						else if(board[i][j] == Constant.WHITE_PIECE)
							WHITE_IS_DEAD = true;
					}
				}
			}
		
		
		return true;
	}
	
	private static void flow(char[][] board, int row, int col, 
			int target_i, int target_j, char target_piece) {
		
		if( target_i>=0 && target_i<row && target_j>=0 && target_j<col){
			
			if(libretyTable[target_i][target_j] == MARKED)
				return;
			
			if(target_piece == Constant.EMPTY_PIECE){
				libretyTable[target_i][target_j] = MARKED;
				flow(board, row, col, target_i+1, target_j, board[target_i][target_j]);
				flow(board, row, col, target_i-1, target_j, board[target_i][target_j]);
				flow(board, row, col, target_i, target_j+1, board[target_i][target_j]);
				flow(board, row, col, target_i, target_j-1, board[target_i][target_j]);
			}else if(board[target_i][target_j] == target_piece){
				libretyTable[target_i][target_j] = MARKED;
				flow(board, row, col, target_i+1, target_j, target_piece);
				flow(board, row, col, target_i-1, target_j, target_piece);
				flow(board, row, col, target_i, target_j+1, target_piece);
				flow(board, row, col, target_i, target_j-1, target_piece);
			}
		}
	}

}
