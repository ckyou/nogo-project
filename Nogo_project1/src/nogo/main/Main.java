package nogo.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

import nogo.utility.CheckLiberty;
import nogo.utility.Constant;
import nogo.utility.Utility;

public class Main {

	static char[][] board;
	static int row;
	static int col;
	
	static int numOfBlackPiece, numOfWhitePiece;
	
	static HashMap<Long, Character> endGameBoard;
	
	final static boolean INPUT_FROM_FILE = true;
	final static boolean OUTPUT_TO_FILE = true;
	static File resultFile;
		
	public static void main(String[] args) {		
		
		buildEndGameTableByGenV2();
		loadEndGameTable();
		try {
			getInput(INPUT_FROM_FILE);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
//		buildEndGameTable();
//		loadEndGameTable();
//		getInput();
	}
	
	public static void getInput(boolean fromeFile) throws IOException{
		FileWriter fwriter = null;
		Scanner sc = null;
		if(fromeFile){
			System.out.println("get input file> ");
			sc=new Scanner(System.in);
			String filePath = sc.nextLine();
			try {
				sc=new Scanner(new File(filePath));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		else{
			sc=new Scanner(System.in);
			System.out.println("get input> ");
		}
		
		if(OUTPUT_TO_FILE){
			resultFile = new File("./result");
			try {
				fwriter=new FileWriter(resultFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		

		while(true){
			
			if(fromeFile && !sc.hasNextLine())
				break;
			
			/* initialize */
			numOfBlackPiece = 0; 
			numOfWhitePiece = 0;
			
			/* get input */
			String temp = sc.next();
			if(temp.equals("exit")) break;
			col = Integer.parseInt(temp);
			temp = sc.next();
			if(temp.equals("exit")) break;
			row = Integer.parseInt(temp);
			String pattern = sc.nextLine(); 
			
			/* fill the board*/
			fillInBoard(pattern.trim(), row, col);
			
			/* check piece number is legal or not */
			if (numOfBlackPiece < numOfWhitePiece){
				if(OUTPUT_TO_FILE)
					fwriter.append("I\n");
				else
					System.out.println("I");
				continue;
			}
			
			/* check board is legal or not */
			if (!CheckLiberty.isLegalBoard(board, row, col)){
				if(OUTPUT_TO_FILE)
					fwriter.append("I\n");
				else
					System.out.println("I");
				continue;
			}
			
			
			int next;
			/* print out who to move next,  */
			if (numOfBlackPiece == numOfWhitePiece){
				if(OUTPUT_TO_FILE)
					fwriter.append("B ");
				else
					System.out.print("B ");
				next = Constant.BLACK;
			}
			else{
				if(OUTPUT_TO_FILE)
					fwriter.append("W ");
				else
					System.out.print("W ");
				next = Constant.WHITE;
			}
			

			boolean win=false;
			long target = TerminalBoardGenerator.getBoardInHashKey(board, row, col);
			Set<Long> set = endGameBoard.keySet();
			Iterator<Long> itr = set.iterator();
			Long key;
			Character value;
			int endgameCnt=0;
			
		    while (itr.hasNext()) {
		    	key = itr.next();
		    	
//		    	System.out.print(TerminalBoardGenerator.translateFromKeyToPattern(key, 3, 3));
//		    	System.out.println(endGameBoard.get(key));
				if ((target & key) == target){
					++endgameCnt;
					value = endGameBoard.get(key);
					if(value == Utility.getPiece(next)){
						win = true;
					}
				}
		    }

		    if(endgameCnt == 0)
		    	System.out.println("err");
		    
		    if(win){
				if(OUTPUT_TO_FILE)
					fwriter.append("win\n");
				else
					System.out.println("win");
		    }
		    else{
				if(OUTPUT_TO_FILE)
					fwriter.append("lose\n");
				else
					System.out.println("lose");
		    }
		}
		
		if(fwriter!=null){
			fwriter.close();
		}
		System.out.println("bye bye~");
	}
	
	
	public static void fillInBoard(String pattern, int row, int col){
		board = new char[row][col];

		int k=0;
		for(int i=0; i<row; ++i){
			for(int j=0; j<col; ++j){
				board[i][j] = pattern.charAt(k);
				
				if (pattern.charAt(k) == Constant.BLACK_PIECE)
					++numOfBlackPiece;
				else if (pattern.charAt(k) == Constant.WHITE_PIECE)
					++numOfWhitePiece;
				
				++k;
				
				if(board[i][j]==' '){
					try {
						throw new Exception("input format error!");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			++k; //jump one character;
		}
	}

	public static void loadEndGameTable(){
		endGameBoard = new HashMap<>();
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader("./"+row+"-"+col));
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] parts = line.split(" ");
				endGameBoard.put(Long.parseLong(parts[0]), parts[1].charAt(0));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}


//		Set<Long> set = endGameBoard.keySet();
//		Iterator<Long> itr = set.iterator();
//		Long key;
//	    while (itr.hasNext()) {
//	    	key = itr.next();
//	    	System.out.print(TerminalBoardGenerator.translateFromKeyToPattern(key, row, col));
//	    	System.out.println("  "+key+" "+endGameBoard.get(key));
//	    }
	}
	
	public static void buildEndGameTable(){
		
		Scanner sc=new Scanner(System.in);
		System.out.print("builld table> ");
		String temp = sc.next();
		col = Integer.parseInt(temp);
		temp = sc.next();
		row = Integer.parseInt(temp);
		
    	double startTime,endTime,totTime;
    	startTime = System.currentTimeMillis();
		TerminalBoardGenerator generator = new TerminalBoardGenerator(row, col);
		generator.gen();
		endTime = System.currentTimeMillis();
		totTime = endTime - startTime;
		System.out.println("Using Time: " + totTime/1000+" sec");
	}
	
	public static void buildEndGameTableByGenV2(){
		
		Scanner sc=new Scanner(System.in);
		System.out.print("builld table> ");
		String temp = sc.next();
		col = Integer.parseInt(temp);
		temp = sc.next();
		row = Integer.parseInt(temp);
		
		if(col == -1 || row == -1){
			System.out.println();
			System.out.print("load table> ");
			temp = sc.next();
			col = Integer.parseInt(temp);
			temp = sc.next();
			row = Integer.parseInt(temp);
			return;
		}
		
    	double startTime,endTime,totTime;
    	startTime = System.currentTimeMillis();
		TerminalBoardGenerator generator = new TerminalBoardGenerator(row, col);
		generator.genV2();
		endTime = System.currentTimeMillis();
		totTime = endTime - startTime;
		System.out.println("Using Time: " + totTime/1000+" sec");
	}
	
	private static class Pair{
		long key; char value;
		Pair(long key, char value){
			this.key = key;
			this.value = value;
		}
	}
}
