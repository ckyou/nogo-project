package nogo.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import nogo.utility.CheckLiberty;
import nogo.utility.Constant;
import nogo.utility.Utility;

public class TerminalBoardGenerator {
	
	int row, col;
	char[][] board;
	HashMap<Long, Character> endGameBoard;
	HashMap<Long, Character> endGameBoardTemp;
	
	long maskBlack=0;
	
	TerminalBoardGenerator(int row, int col) {
		this.row = row;
		this.col = col;
		board = new char[row][col];

		for (int i = 0; i < row; ++i)
			for (int j = 0; j < col; ++j){
				board[i][j] = Constant.EMPTY_PIECE;
			}

		endGameBoard = new HashMap<>();
		
		maskBlack=0;
		for(int i=0; i<row*col; ++i){
			maskBlack <<=2;
			maskBlack |= 1;
		}
	}
	
	private void writeTableToFile(){
		File database =new File("./"+row+"-"+col);
		try {
			FileWriter fwriter=new FileWriter(database);
			
			Set<Long> set = endGameBoard.keySet();
			Iterator<Long> itr = set.iterator();
			Long key;
		    while (itr.hasNext()) {
		    	key = itr.next();
		    	fwriter.write(key+" "+endGameBoard.get(key));
		    	fwriter.write('\n');
		    }
		    
		    fwriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void gen() {
		_gen(Constant.BLACK, 1);
		writeTableToFile();
	}

	private void _gen(int who, int piece) {
		boolean haveToPass = true;

		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				
				if (board[i][j] == Constant.EMPTY_PIECE) {
					
					board[i][j] = Utility.getPiece(who);
					if (CheckLiberty.isLegalBoard(board, row, col)) {
						haveToPass = false;
						
						_gen(Utility.opponent(who), piece+1);						
						board[i][j] = Constant.EMPTY_PIECE;

					}else
						board[i][j] = Constant.EMPTY_PIECE;

				}
			}
		}

		if (haveToPass) {
			Long key = getBoardInHashKey();
			Character value = endGameBoard.get(key);

			if (value != null && value != Utility.opponent(who)) {
//				System.out.print("error ");
//				printInLine();
//				System.out.println();
			}

			if (value == null) {
				endGameBoard.put(key, Utility.getPiece(Utility.opponent(who)));
			}
		}
	}
	
	private Vector<String> blackCombin, whiteCombin;
	
	public void genV2(){
		blackCombin = new Vector<>();
		whiteCombin = new Vector<>();
		int bPieceNum = row * col / 2;
		int wPieceNum = ((row * col) % 2 == 0)?  bPieceNum - 1 : bPieceNum;

		int bPieceNum_bak = 0, wPieceNum_bak=0;
		
		boolean stop = true;
		
		while(true)
		{
//			System.out.print("perm B");
			if(bPieceNum!=bPieceNum_bak){
				blackCombin.clear();
//				permutation("", getMaskString(row * col, bPieceNum), Constant.BLACK);
				combination(Constant.BLACK, "", row * col, bPieceNum);
				bPieceNum_bak = bPieceNum;
				
				
				whiteCombin.clear();
//				permutation("", getMaskString(row * col - bPieceNum, wPieceNum), Constant.WHITE);
				combination(Constant.WHITE, "", row * col - bPieceNum, wPieceNum);
				wPieceNum_bak = wPieceNum;
			}
//			System.out.println("..OK");
//			System.out.print("perm W");
			if(wPieceNum!=wPieceNum_bak){
				whiteCombin.clear();
//				permutation("", getMaskString(row * col - bPieceNum, wPieceNum), Constant.WHITE);
				combination(Constant.WHITE, "", row * col - bPieceNum, wPieceNum);
				wPieceNum_bak = wPieceNum;
			}
//			System.out.println("..OK");
			// try each pattern
			for(int p=0; p<blackCombin.size(); ++p)
			{
				for(int q=0; q<whiteCombin.size(); ++q)
				{
					fillBoardFromPattern(blackCombin.get(p), whiteCombin.get(q));
					
//					System.out.print("try ");
//					printInLine();
//					System.out.println();
					
					//check is legal or not
					if(CheckLiberty.isLegalBoard(board, row, col))
					{
						
						boolean finded;
						int who; 
						if(bPieceNum > wPieceNum)
							who = Constant.WHITE;
						else
							who = Constant.BLACK;
	
						finded = genV2_checkEnd(who);
						if(!finded){
							stop=false;
							Long key = getBoardInHashKey();
							Character value = endGameBoard.get(key);
							if (value == null) {
//								printInLine();
//								System.out.println();
								endGameBoard.put(key, Utility.getPiece(Utility.opponent(who)));
							}
						}
					}					
				}
			}
			
			if(stop)
				break;
			
			if(bPieceNum > wPieceNum)
				--bPieceNum;			
			else
				--wPieceNum;	
			
			stop = true;

		}
		writeTableToFile();
	}
	
	private boolean genV2_checkEnd(int who){
		boolean finded = false;
		
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				
				if (board[i][j] == Constant.EMPTY_PIECE) {
					
					board[i][j] = Utility.getPiece(who);
					if (CheckLiberty.isLegalBoard(board, row, col)) {
						finded = true;
						board[i][j] = Constant.EMPTY_PIECE;
						return finded;
					}
					board[i][j] = Constant.EMPTY_PIECE;
				}
			}
		}
		
		return finded;
	}
	
	private void fillBoardFromPattern(String blackPattern, String whitePattern){
		
		for(int i=0; i<blackPattern.length(); ++i){
			if(blackPattern.charAt(i)=='1')
				board[i/col][i%col] = Constant.BLACK_PIECE;
			else
				board[i/col][i%col] = Constant.EMPTY_PIECE;
		}
			
		
		int k=0;
		for(int i=0; i<row; ++i){
			for(int j=0; j<col; ++j){
				if(board[i][j] == Constant.EMPTY_PIECE){
					if(whitePattern.charAt(k)=='1')
						board[i][j] = Constant.WHITE_PIECE;
					++k;
					
					if(k >=  whitePattern.length())
						return;
				}
			}
		}
	}
	
	private String getMaskString(int n, int r){
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<r; ++i)
			sb.append('1');
		for(int i=0; i<n-r; ++i)
			sb.append('0');
		
		return sb.toString();
	}
	
	private void combination(int who, String str, int n, int r){
		
		if(r==0){
//			System.out.println("in "+str);
			while(str.length()!=n)
				str +="0";
			
			//push to list
	    	if(who == Constant.BLACK && !blackCombin.contains(str))
	    		blackCombin.add(str);
	    	else if(who == Constant.WHITE && !whiteCombin.contains(str))
	    		whiteCombin.add(str);
		}
		
		if(str.length() == n)
			return;
		
		combination(who, str+"1", n, r-1);
		combination(who, str+"0", n, r);
		
	}
	
	
	/**
	 * http://stackoverflow.com/questions/4240080/generating-all-permutations-of-a-given-string
	 * @param prefix
	 * @param str
	 * @param who
	 */
	private void permutation(String prefix, String str, int who) {
	    int n = str.length();
	    if (n == 0){
	    	if(who == Constant.BLACK && !blackCombin.contains(prefix))
	    		blackCombin.add(prefix);
	    	else if(who == Constant.WHITE && !whiteCombin.contains(prefix))
	    		whiteCombin.add(prefix);
	    }
	    else {
	        for (int i = 0; i < n; i++)
	            permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i+1, n), who);
	    }
	}
	
	private void setDontGoList(Vector<PieceLoc> list){
		list.clear();
		long target = getBoardInHashKey(board, row, col);
		target &= maskBlack;
		
		Set<Long> set = endGameBoard.keySet();
		Iterator<Long> itr = set.iterator();
		Long key;
	    while (itr.hasNext()) {
	    	key = itr.next() & maskBlack;
	    	if((key & target) == target){
	    		key ^= target;
	    		for(int i=row-1; i>=0; --i)
	    			for(int j=col-1; j>=0; --j){
	    				if((key&1) == 1){
	    					list.add(new PieceLoc(i,j));
	    				}
	    			}
	    	}
	    }
	}
	
	class PieceLoc{
		public int i, j;
		PieceLoc(int i, int j){
			this.i=i;
			this.j=j;
		}
	}
	
	private long getBoardInHashKey() {
		return getBoardInHashKey(this.board, row, col);
	}

	public static long getBoardInHashKey(char[][] board, int row, int col) {
		long key = 0;

		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				key <<= 2;
				if (board[i][j] == Constant.BLACK_PIECE)
					key |= Constant.BLACK;
				else if (board[i][j] == Constant.WHITE_PIECE)
					key |= Constant.WHITE;
				else
					key |= Constant.EMPTY;

			}
		}

		return key;
	}

	public void print() {
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j)
				System.out.print(board[i][j]);

			System.out.println();
		}
	}

	public void printInLine() {
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j)
				System.out.print(board[i][j]);

			System.out.print(" ");
		}
	}

	public static String translateFromKeyToPattern(long key, int row, int col){
		char[][] board = new char[row][col];
		
		long piece;
		for(int i=row-1; i>=0; --i){
			for(int j=col-1; j>=0; --j){
				piece = 3 & key;
				
				if(piece  == Constant.BLACK)
					board[i][j] = Constant.BLACK_PIECE;
				else if(piece  == Constant.WHITE)
					board[i][j] = Constant.WHITE_PIECE;
				else if(piece  == Constant.EMPTY)
					board[i][j] = Constant.EMPTY_PIECE;
				else
					board[i][j] = 'E';
				
				key >>= 2;
			}
		}
		
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j)
				builder.append(board[i][j]);

			builder.append(" ");
		}
		
		return builder.toString();
	}
	
}
