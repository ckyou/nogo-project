package nogo.utility;

public class Constant {
	public static int BLACK = 1;
	public static int WHITE = 2;
	public static int EMPTY = 0;
	public static int NONEMPTY = -1;

	public static char BLACK_PIECE = '@';
	public static char WHITE_PIECE = 'O';
	public static char EMPTY_PIECE = '.';

	public static int DEFAULT_ROW_SIZE = 9;
	public static int DEFAULT_COL_SIZE = 9;
	
	public static String PROGRAM_VERSION = "1.0";
	public static String PROGRAM_NAME = "0156026_NOGO_bot";
	
}
