package nogo.utility;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Logger {
	
	public final static String LOGFILE_LOCATION="./nogo_log";
	
	private Logger(){
	}

	public static void e(String text){
		PrintWriter out = null;
		try {
			out = new PrintWriter(
					new BufferedWriter(
							new FileWriter(LOGFILE_LOCATION, true)));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		if(out!=null){
			out.println(System.currentTimeMillis()+": "+text);
			out.close();
		}
	}
}
