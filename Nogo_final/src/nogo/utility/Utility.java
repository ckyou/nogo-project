package nogo.utility;


public class Utility {
	public static int opponent(int you) {
		if (you == Constant.BLACK)
			return Constant.WHITE;
		if (you == Constant.WHITE)
			return Constant.BLACK;

		try {
			throw new Exception("illegal input: not BLACK or WHITE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return you;
	}

	public static char opponentPiece(char you) {
		if (you == Constant.BLACK_PIECE)
			return Constant.WHITE_PIECE;
		if (you == Constant.WHITE_PIECE)
			return Constant.BLACK_PIECE;

		try {
			throw new Exception("illegal input: not BLACK or WHITE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return you;
	}

	public static char getPiece(int you) {
		if (you == Constant.BLACK)
			return Constant.BLACK_PIECE;
		if (you == Constant.WHITE)
			return Constant.WHITE_PIECE;

		try {
			throw new Exception("illegal input: not BLACK or WHITE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Constant.EMPTY_PIECE;
	}

	public static void printBoard(char[][] board, int row, int col) {
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j)
				System.out.print(board[i][j]);

			System.out.println();
		}
	}

	public static void printBoardInLine(char[][] board, int row, int col) {
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j)
				System.out.print(board[i][j]);

			System.out.print(" ");
		}
	}

	public static String getBoardInLine(char[][] board, int row, int col) {
		StringBuffer tmp = new StringBuffer();
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j)
				tmp.append(board[i][j]);

			tmp.append(" ");
		}
		return tmp.toString();
	}
}
