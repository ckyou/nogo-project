package nogo.main;

import java.util.HashMap;
import java.util.Scanner;

import nogo.mct.Location;
import nogo.mct.TreeNode;
import nogo.mct.UCTSearch;
import nogo.mct.utility.MoveTable;
import nogo.mct.utility.MoveTableGenerator;
import nogo.utility.CheckLiberty;
import nogo.utility.Constant;
import nogo.utility.Logger;
import nogo.utility.Utility;

public class Main {

	static char[][] board;
	static int row = Constant.DEFAULT_ROW_SIZE;
	static int col = Constant.DEFAULT_COL_SIZE;
	static int numOfBlackPiece, numOfWhitePiece;
	
	final static HashMap<Character, Integer> XReverseMapping;
	static{
		XReverseMapping = new HashMap<>();
		XReverseMapping.put('A', 1);
		XReverseMapping.put('B', 2);
		XReverseMapping.put('C', 3);
		XReverseMapping.put('D', 4);
		XReverseMapping.put('E', 5);
		XReverseMapping.put('F', 6);
		XReverseMapping.put('G', 7);
		XReverseMapping.put('H', 8);
		XReverseMapping.put('J', 9);
		XReverseMapping.put('a', 1);
		XReverseMapping.put('b', 2);
		XReverseMapping.put('c', 3);
		XReverseMapping.put('d', 4);
		XReverseMapping.put('e', 5);
		XReverseMapping.put('f', 6);
		XReverseMapping.put('g', 7);
		XReverseMapping.put('h', 8);
		XReverseMapping.put('j', 9);
	}
	
	final static HashMap<Integer, Character> XMapping;
	static{
		XMapping = new HashMap<>();
		XMapping.put(1, 'a');
		XMapping.put(2, 'b');
		XMapping.put(3, 'c');
		XMapping.put(4, 'd');
		XMapping.put(5, 'e');
		XMapping.put(6, 'f');
		XMapping.put(7, 'g');
		XMapping.put(8, 'h');
		XMapping.put(9, 'j');
	}
		
	public static void main(String[] args) {	
		
		getInput();
	}
	
	public static void kgsGtpReply(){
		System.out.println("=\n");
	}
	
	public static void kgsGtpReply(String message){
		System.out.println("="+message+"\n");
	}
	
	public static void kgsGtpReply_(String message){
		System.out.println("="+message);
	}

	public static void kgsGtpReply(int message){
		System.out.println("="+message+"\n");
	}
	
	public static void getInput(){
		Scanner sc=new Scanner(System.in);

		/* init board*/
		_initBoard();
		String input = null;
		
		while(true)
		{
			/* get input */
			input = sc.nextLine();
			String[] tokens = input.split(" ");

			if(tokens[0].equals("play") && tokens.length == 3){
				int x = XReverseMapping.get(tokens[2].charAt(0)) - 1, 
					y = Integer.parseInt("" + tokens[2].charAt(1)) - 1;
				
				if(board[x][y] != Constant.EMPTY_PIECE){
					Logger.e("play to a non-empty location");
					continue;
				}
				
				if(tokens[1].equals("b") || tokens[1].equals("B") || tokens[1].equals("black")){
					board[x][y] = Constant.BLACK_PIECE;
					++numOfBlackPiece;
					MoveTableGenerator.update(x, y);
					kgsGtpReply();
				}else if(tokens[1].equals("w") || tokens[1].equals("W") || tokens[1].equals("white")){
					board[x][y] = Constant.WHITE_PIECE;
					++numOfWhitePiece;
					MoveTableGenerator.update(x, y);
					kgsGtpReply();
				}else{
					Logger.e("Syntax ERROR at parsing play");
				}
				
				/*if(!_isPieceNumberLegal()){
					Logger.e("Sequnce is wrong!! " 
							+"-> piece-count w:"+numOfWhitePiece
							+" b:"+numOfBlackPiece);
					
					
					Logger.e("-> undo this move");
					board[x][y] = Constant.EMPTY_PIECE;
					if(tokens[1].equals("black")){
						--numOfBlackPiece;
					}else if(tokens[1].equals("white")){
						--numOfWhitePiece;
					}
				}*/

			}else if(tokens[0].equals("genmove") && tokens.length == 2){

				TreeNode root;
				
				if(tokens[1].equals("b") || tokens[1].equals("B") || tokens[1].equals("black")){
					root = new TreeNode(new Location(-1, -1), Constant.WHITE);
					UCTSearch.apply(root, board);
					Location bestMove = getBestMove(root);
						
					if(bestMove!=null){	
						++numOfBlackPiece;
						board[bestMove.x][bestMove.y]=Constant.BLACK_PIECE;	//do move
						MoveTableGenerator.update(bestMove.x, bestMove.y);
						kgsGtpReply(""+XMapping.get(bestMove.x+1)+(bestMove.y+1));	
					}else{
						Logger.e("get null bestMove fron "+Utility.getBoardInLine(board, row, col));
						kgsGtpReply("resign");
					}

				}else if(tokens[1].equals("w") || tokens[1].equals("W") || tokens[1].equals("white")){
					root = new TreeNode(new Location(-1, -1), Constant.BLACK);
					UCTSearch.apply(root, board);
					Location bestMove = getBestMove(root);
					if(bestMove!=null){	
						++numOfWhitePiece;
						board[bestMove.x][bestMove.y]=Constant.WHITE_PIECE;	//do move
						MoveTableGenerator.update(bestMove.x, bestMove.y);
						kgsGtpReply(""+XMapping.get(bestMove.x+1)+(bestMove.y+1));
					}else{
						Logger.e("get null bestMove fron "+Utility.getBoardInLine(board, row, col));
						kgsGtpReply("resign");
					}
					
				}else{
					Logger.e("Syntax ERROR at parsing genmove");
				}
								
			}else{
				
				if(tokens[0].equals("quit")){ 
					kgsGtpReply();	
					break;
				}else if(tokens[0].equals("version")){
					kgsGtpReply(Constant.PROGRAM_VERSION);
				}else if(tokens[0].equals("protocol_version")){
					kgsGtpReply(2);
				}else if(tokens[0].equals("name")){
					kgsGtpReply(Constant.PROGRAM_NAME);
				}else if(tokens[0].equals("boardsize") && tokens.length == 2){
					if(tokens[1].equals("9"))
						kgsGtpReply();	
					else
						kgsGtpReply("unacceptable size");	
				}else if(tokens[0].equals("clear_board") || tokens[0].equals("clearboard")){
					_initBoard();
					kgsGtpReply();	
				}else if(tokens[0].equals("list_commands") || tokens[0].equals("help")){
					kgsGtpReply_("quit");
					kgsGtpReply_("version");
					kgsGtpReply_("protocol_version");
					kgsGtpReply_("name");
					kgsGtpReply_("boardsize");
					kgsGtpReply_("clear_board");
					kgsGtpReply_("clearboard");
					kgsGtpReply_("komi");
					kgsGtpReply_("play");
					kgsGtpReply("genmove");
				}else{
					//Logger.e("Syntax ERROR, not supoort command: "+tokens[0]);
					//we just return , no matter what input
					kgsGtpReply();
				}
			}
		}

		sc.close();
	}	
	
	
	
	public static Location getBestMove(TreeNode root){
		float score=0, tmpScore=0;
		float bestMoveNum=0;
		
		Location bestMove = null;
		for(int i=0; i<root.children.size(); ++i){
			tmpScore = root.children.get(i).winTime/root.children.get(i).visitedTime;
//			if(tmpScore > score ){
//				score = tmpScore;
//				bestMove = root.children.get(i).location;
//			}
			if(root.children.get(i).visitedTime > bestMoveNum){
				bestMoveNum = root.children.get(i).visitedTime;
				bestMove = root.children.get(i).location;
				//System.out.println(""+bestMove.x+"/"+bestMove.y+" "+bestMoveNum);
			}
		}
		return bestMove;
	}
	
	private static boolean _isPieceNumberLegal(){
		if(numOfBlackPiece < numOfWhitePiece)
			return false;
		
		if(numOfBlackPiece > numOfWhitePiece+1)
			return false;
		
		return true;
	}
	
	private static void _initBoard(){
		board = new char[row][col];
		numOfBlackPiece = 0;
		numOfWhitePiece = 0;
		for(int i=0; i<row; ++i)
			for(int j=0; j<col; ++j)
				board[i][j] = Constant.EMPTY_PIECE;
		
		//initialize move table
		MoveTableGenerator.initialize();
	}
	
	public static void fillInBoard(String pattern, int row, int col){
		board = new char[row][col];
		numOfBlackPiece = 0;
		numOfWhitePiece = 0;

		int k=0;
		for(int i=0; i<row; ++i){
			for(int j=0; j<col; ++j){
				board[i][j] = pattern.charAt(k);
				
				if (pattern.charAt(k) == Constant.BLACK_PIECE)
					++numOfBlackPiece;
				else if (pattern.charAt(k) == Constant.WHITE_PIECE)
					++numOfWhitePiece;
				
				++k;
				
				if(board[i][j]==' '){
					try {
						throw new Exception("input format error!");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			++k; //jump one character;
		}
	}
		
}
