package nogo.mct;

public class Location {
	public static final int TYPE_EMPTY = -1;
	public static final int TYPE_NOGO = 0;
	public static final int TYPE_1GO = 1;
	public static final int TYPE_2GO = 2;
	public static final int TYPE_E1GO = 3;

	public static final int TYPE_BNE_1GO = 4;
	public static final int TYPE_BE_1GO = 5;
	public static final int TYPE_WNE_1GO = 6;
	public static final int TYPE_WE_1GO = 7;
	
	public int x, y;
	public int type;
	
	public Location(int locX, int locY){
		this.x = locX;
		this.y = locY;
		this.type = TYPE_EMPTY;
	}
	
	public Location(int locX, int locY, int type){
		this.x = locX;
		this.y = locY;
		this.type = type;
	}
	
	public boolean equals(Location target){
		return this.x == target.x &&  this.y == target.y;
	}
}
