package nogo.mct.utility;

import nogo.mct.Location;
import nogo.utility.Constant;

public class MoveTableGenerator {
	
	private final static char WALL = '|';
	public static int[][] mTable;	//this table indicate how many empty squares surrounding
	private static int[][] cTable;
	
	public static void initialize(){
		mTable = new int[Constant.DEFAULT_ROW_SIZE][Constant.DEFAULT_COL_SIZE];
		cTable = new int[Constant.DEFAULT_ROW_SIZE][Constant.DEFAULT_COL_SIZE];
		
		for(int i=0; i<Constant.DEFAULT_ROW_SIZE; ++i){
			for(int j=0; j<Constant.DEFAULT_COL_SIZE; ++j){
				mTable[i][j] = 4;
				cTable[i][j] = Constant.EMPTY;
				if (i == 0)
					mTable[i][j] -= 1;
				if (j == 0)
					mTable[i][j] -= 1;
				if (i == Constant.DEFAULT_ROW_SIZE - 1)
					mTable[i][j] -= 1;
				if (j == Constant.DEFAULT_COL_SIZE - 1)
					mTable[i][j] -= 1;
			}
		}
	}
	
	public static void update(int x, int y){
		mTable[x][y]=0;
		cTable[x][y] = Constant.NONEMPTY;
		
		if (x != 0 && mTable[x-1][y]!=0)
			mTable[x-1][y]-=1;
		if (x < Constant.DEFAULT_ROW_SIZE - 1 && mTable[x+1][y]!=0)
			mTable[x+1][y]-=1;
		if (y != 0 && mTable[x][y-1]!=0)
			mTable[x][y-1]-=1;
		if (y < Constant.DEFAULT_COL_SIZE - 1 && mTable[x][y+1]!=0)
			mTable[x][y+1]-=1;
	}
	
	public static void undo(int x, int y){
		cTable[x][y] = Constant.EMPTY;
		
		if (x != 0 && cTable[x-1][y]==Constant.EMPTY){
			mTable[x-1][y]+=1;
			++mTable[x][y];
		}
		if (x < Constant.DEFAULT_ROW_SIZE - 1 && cTable[x+1][y]==Constant.EMPTY){
			mTable[x+1][y]+=1;
			++mTable[x][y];
		}
		if (y != 0 && cTable[x][y-1]==Constant.EMPTY){
			mTable[x][y-1]+=1;
			++mTable[x][y];
		}
		if (y < Constant.DEFAULT_COL_SIZE - 1 && cTable[x][y+1]==Constant.EMPTY){
			mTable[x][y+1]+=1;
			++mTable[x][y];
		}

	}
	
	public static MoveTable gen(char[][] board, char myPiece) {
		MoveTable table = new MoveTable();

		for (int i = 0; i < Constant.DEFAULT_ROW_SIZE; ++i) {
			for (int j = 0; j < Constant.DEFAULT_COL_SIZE; ++j) {
				
				if (board[i][j] != Constant.EMPTY_PIECE)
					continue;

				if(mTable[i][j] == 4 || mTable[i][j] == 3 || mTable[i][j] == 2){
					//TYPE_2GO also means this position is legal
					// 4 or 3 or 2 means the number of surroundeding empty squares
					table.move2Go.add(new Location(i, j, Location.TYPE_2GO));
					
				}else if(mTable[i][j] == 1){
					//TODO: it seems we can do more here
					table.move1Go.add(new Location(i, j, Location.TYPE_1GO));
					
				}else if(mTable[i][j] == 0){
					int tmpCnt=4;
					
					if (i != 0 && board[i-1][j] != myPiece)
						--tmpCnt;
					if (i < Constant.DEFAULT_ROW_SIZE - 1 &&  board[i+1][j] != myPiece)
						--tmpCnt;
					if (j != 0 && board[i][j-1] != myPiece)
						--tmpCnt;
					if (j < Constant.DEFAULT_COL_SIZE - 1 && board[i][j+1] != myPiece)
						--tmpCnt;
					
					if(tmpCnt==4)
						table.moveE1Go.add(new Location(i, j, Location.TYPE_E1GO));
					else
						table.move1Go.add(new Location(i, j, Location.TYPE_1GO));
				}else {
					table.move1Go.add(new Location(i, j, Location.TYPE_1GO));
				}
			}
			//System.out.println();
		}
		return table;
	}
	
	public static void printMTable(){
		for(int i=0; i<Constant.DEFAULT_ROW_SIZE; ++i){
			for(int j=0; j<Constant.DEFAULT_COL_SIZE; ++j){
				System.out.print(mTable[i][j]);
			}
			System.out.println();
		}
	}
	
}
