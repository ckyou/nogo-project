package nogo.mct;

import java.util.ArrayList;
import java.util.Random;

import nogo.mct.utility.MoveTable;
import nogo.mct.utility.MoveTableGenerator;
import nogo.utility.CheckLiberty;
import nogo.utility.Constant;
import nogo.utility.Utility;

public class UCTSearch {
	public static final int MAX_ITERATION_TIME = 11000; // in millisecond
	public static final double SQRT_2 = 1.414; 

	private static MoveTable moveTable;
	private static Random random;
	private static long beginTime, endTime;
	private static TreeNode rootNode;

	public static boolean apply(TreeNode root, char[][] board) {
		random = new Random();

		beginTime = System.currentTimeMillis();
		endTime = beginTime + MAX_ITERATION_TIME;
		
		rootNode = root;
		
		while (System.currentTimeMillis() < endTime) {
			TreeNode target = getTarget(board, root);
			if (target == null)
				break;

			int whoWin = playout(target, board);
			int result;
			if (whoWin == root.player)
				result = 0;
			else
				result = 1;
			backPropagation(target, result);
		}

		return true;
	}

	private static TreeNode getTarget(char[][] board, TreeNode pointer) {
		TreeNode target = null;

		if (pointer == null)
			return target;

		moveTable = MoveTableGenerator.gen(board, Utility.getPiece(Utility.opponent(pointer.player)));
		
		if (pointer.child2GoNum < moveTable.move2Go.size()) {
			int index = random.nextInt(moveTable.move2Go.size());
			target = expand(pointer, moveTable.move2Go.get(index));
			
		} else if (pointer.child1GoNum < moveTable.move1Go.size() + moveTable.moveE1Go.size()) {
			
			int index = 0;
			boolean finded = false;
			
			while (!finded && System.currentTimeMillis() < endTime) {
				if(moveTable.move1Go.size() == 0)
					break;
					
				index = random.nextInt(moveTable.move1Go.size());

				board[moveTable.move1Go.get(index).x][moveTable.move1Go.get(index).y] 
						= Utility.getPiece(Utility.opponent(pointer.player));
				if (CheckLiberty.isLegalBoard(board, Constant.DEFAULT_ROW_SIZE,Constant.DEFAULT_COL_SIZE))
					finded = true;
				board[moveTable.move1Go.get(index).x][moveTable.move1Go.get(index).y] 
						= Constant.EMPTY_PIECE;
				
				if(!finded)	//not found, it mean this pos is illegal, so we remove it
					moveTable.move1Go.remove(index);
			}
			
			if(finded)
				target = expand(pointer, moveTable.move1Go.get(index));
			else if(moveTable.moveE1Go.size()!=0)
				target = expand(pointer, moveTable.moveE1Go.get(random.nextInt(moveTable.moveE1Go.size())));
			else{
				//means no illegal 1Go child
				pointer.child1GoNum = moveTable.move1Go.size() + moveTable.moveE1Go.size();
				//then try again
				return pointer;
			}
				
		} else {
			TreeNode bestChild = findBestChild(pointer);
			if(bestChild == null)	//if run in this if, it will be very strange!!
				return target;
			
			MoveTableGenerator.update(bestChild.location.x, bestChild.location.y);
			target = getTarget(board, bestChild);
			MoveTableGenerator.undo(bestChild.location.x, bestChild.location.y);
		}

		return target;
	}

	private static TreeNode findBestChild(TreeNode pointer) {
		TreeNode bestChild = null;
		double bestUCT = 0;
		double tmp = 0;

		for (int i = 0; i < pointer.children.size(); ++i) {
			tmp = pointer.children.get(i).winTime
					/ pointer.children.get(i).visitedTime 
					+ SQRT_2 * Math.sqrt(Math.log(rootNode.visitedTime)/pointer.children.get(i).visitedTime) ;
			if (tmp >= bestUCT) {
				bestChild = pointer.children.get(i);
				bestUCT = tmp;
			}
		}
		return bestChild;
	}

	private static TreeNode expand(TreeNode parent, Location move) {
		TreeNode target = new TreeNode(move, Utility.opponent(parent.player));
		target.parent = parent;
		parent.children.add(target);

		if (move.type == Location.TYPE_2GO) {
			++parent.child2GoNum;

		} else if (move.type == Location.TYPE_1GO || move.type == Location.TYPE_E1GO) {
			++parent.child1GoNum;
		}

		return target;
	}

	private static int playout(TreeNode fromThisNode, char[][] board) {
		int whoWin = fromThisNode.player;
		TreeNode pointer = fromThisNode;
		/* setup board */
		while (pointer.parent != null) {
			board[pointer.location.x][pointer.location.y] = Utility.getPiece(pointer.player);
			//MoveTableGenerator.update(pointer.location.x, pointer.location.y);
	
			pointer = pointer.parent;
		}

		//whoWin = _playout(board, Utility.opponent(pointer.player));
		
		getEmptySquareList(board);
		whoWin = _playout_random(board, Utility.opponent(fromThisNode.player));
		
		/* reset board */
		pointer = fromThisNode;
		while (pointer.parent != null) {
			board[pointer.location.x][pointer.location.y] = Constant.EMPTY_PIECE;
			//MoveTableGenerator.undo(pointer.location.x, pointer.location.y);
			
			pointer = pointer.parent;
		}

		return whoWin;
	}

	private static ArrayList<Location> eTable;
	private static void getEmptySquareList(char[][] board){
		eTable = new ArrayList<>();
		for(int i=0; i<Constant.DEFAULT_ROW_SIZE; ++i)
			for(int j=0; j<Constant.DEFAULT_COL_SIZE; ++j)
				if(board[i][j]==Constant.EMPTY_PIECE)
					eTable.add(new Location(i, j));
	}	
	
	private static int _playout_random(char[][] board, int player) {
		int whoWin = player;
		boolean finded = false;
		int index, x, y;
		
		while (!finded && System.currentTimeMillis() < endTime) {
			if(eTable.size()==0)
				return whoWin;	//i have no place to put
			
			index = random.nextInt(eTable.size());
			x = eTable.get(index).x;
			y = eTable.get(index).y;
			eTable.remove(index);
			
			board[x][y] = Utility.getPiece(player);
			if (CheckLiberty.isLegalBoard(board, Constant.DEFAULT_ROW_SIZE,Constant.DEFAULT_COL_SIZE)) {
				finded = true;
				whoWin = _playout_random(board, Utility.opponent(player));
			}
			board[x][y] = Constant.EMPTY_PIECE;
			
		}
		
		return whoWin;
	}

	private static int _playout(char[][] board, int player) {
		int whoWin = Utility.opponent(player);
		int index, x, y;
		boolean finded = false, have1EGO=false;
		int times=0;
		
		MoveTable moveTable = MoveTableGenerator.gen(board, Utility.opponentPiece(Utility.getPiece(player)));
		
		if(moveTable.moveE1Go.size() > 0 )
			have1EGO=true;
		
		while (!finded && System.currentTimeMillis() < endTime) {

			if(moveTable.move2Go.size()>0){
				finded = true;
				index = random.nextInt(moveTable.move2Go.size());
				x = moveTable.move2Go.get(index).x;
				y = moveTable.move2Go.get(index).y;
				board[x][y] = Utility.getPiece(Utility.opponent(player));
				whoWin = _playout(board, Utility.opponent(player));
				board[x][y] = Constant.EMPTY_PIECE;
				
			}else if( moveTable.move1Go.size() > 0 ){
				++times;
				index = random.nextInt(moveTable.move1Go.size());
				x = moveTable.move1Go.get(index).x;
				y = moveTable.move1Go.get(index).y;
				board[x][y] = Utility.getPiece(Utility.opponent(player));
				if (CheckLiberty.isLegalBoard(board, Constant.DEFAULT_ROW_SIZE,Constant.DEFAULT_COL_SIZE)) {
					finded = true;
					whoWin = _playout(board, Utility.opponent(player));
				}
				board[x][y] = Constant.EMPTY_PIECE;
				
				if(times>6){
					finded = true;
					if(have1EGO == true){
						index = random.nextInt(moveTable.moveE1Go.size());
						x = moveTable.moveE1Go.get(index).x;
						y = moveTable.moveE1Go.get(index).y;
						board[x][y] = Utility.getPiece(Utility.opponent(player));
						whoWin = _playout(board, Utility.opponent(player));
						board[x][y] = Constant.EMPTY_PIECE;
					}
				}
			}

		}

		return whoWin;
	}

	private static void backPropagation(TreeNode poiner, int result) {
		if (poiner != null) {
			++poiner.visitedTime;
			if (result != 0)
				++poiner.winTime;
			backPropagation(poiner.parent, result);
		}
	}
}
