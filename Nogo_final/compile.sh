#!/bin/sh

find -name "*.java" > sources

if [ ! -d bin ]; then 
    echo "bin folder not found, so mkdir"
    mkdir bin
fi

rm -rf bin/*

javac -d bin/ @sources

rm -f sources

cd bin 
find -name "*.class" > bin_sources

jar cfm ../nogo.jar ../MANIFEST.MF @bin_sources

rm -f bin_sources
rm -rf *
echo "compile finish!"
